package main

import (
	"linescapes/linescape"
	"math"

	"github.com/bit101/blgo"
	"github.com/bit101/blgo/anim"
	"github.com/bit101/blgo/blmath"
	"github.com/bit101/blgo/color"
	"github.com/bit101/blgo/random"
	"github.com/bit101/blgo/util"
)

const width = 400.0
const height = 400.0

var angle = 0.0

func main() {
	// one()
	two()
	util.ConvertToGIF("frames", "out.gif", 30)
	util.ViewImage("out.gif")
}

func two() {
	animation := anim.NewAnimation(400, 400, 5*30)
	animation.Render("frames", "", onFrame)
}

func onFrame(surface *blgo.Surface, percent float64) {
	angle = math.Pi * 2.0 * percent
	surface.ClearRGB(0, 0, 0)
	ls := linescape.NewLinescape(0, -100, width, height+100, 5, 5, color.Black(), color.White(), getOffset2)
	ls.Render(surface)
}

func getOffset2(x, y, w, h float64) float64 {
	offset := 0.0
	for i := 0.0; i < 3.0; i++ {
		offset += getOffsetForAngle(x, y, angle+math.Pi*2.0*i/3.0)
	}
	offset += random.FloatRange(-1, 1)
	return offset
}

func getOffsetForAngle(x, y, a float64) float64 {
	xx := width/2 + math.Cos(a)*100
	yy := height*0.6 + math.Sin(a)*100
	dist := math.Hypot(xx-x, yy-y)
	return -1000 / dist
}

func one() {
	surface := blgo.NewSurface(width, height)
	surface.ClearRGB(0, 0, 0)
	// surface.SetLineWidth(0.9)

	ls := linescape.NewLinescape(0, -100, width, height+100, 3, 3, color.Black(), color.White(), getOffset)
	ls.Render(surface)

	surface.WriteToPNG("out.png")
	util.ViewImage("out.png")
}

func getOffset(x, y, w, h float64) float64 {
	dx := w/2 - x
	dy := h/2 - y
	dist := math.Hypot(dx, dy)
	offset := random.FloatRange(0, 20)
	offset *= blmath.Map(math.Sin(dist*0.08), -1, 1, 0, 1)
	offset += math.Sin(x*0.015) * 70
	offset += math.Sin(y*0.025) * 30
	return offset
}
