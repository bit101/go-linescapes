package gridscape

import (
	"github.com/bit101/blgo"
	"github.com/bit101/blgo/color"
)

type GridScapeOffset func(x, y, w, h float64) float64

type Gridscape struct {
	X         float64
	Y         float64
	W         float64
	H         float64
	Xres      float64
	Yres      float64
	BgColor   color.Color
	FgColor   color.Color
	GetOffset GridScapeOffset
}

func NewGridscape(x, y, w, h, xres, yres float64, bgColor, fgColor color.Color, getOffset GridScapeOffset) *Gridscape {
	return &Gridscape{
		X:         x,
		Y:         y,
		W:         w,
		H:         h,
		Xres:      xres,
		Yres:      yres,
		BgColor:   bgColor,
		FgColor:   fgColor,
		GetOffset: getOffset,
	}
}

func (l *Gridscape) Render(s *blgo.Surface) {
	s.Save()
	s.Rectangle(l.X, l.Y, l.W, l.H)
	s.Clip()
	s.ClearColor(l.BgColor)
	s.SetSourceColor(l.FgColor)
	for y := l.Y; y < l.H+l.H+l.Yres; y += l.Yres {
		for x := l.X; x < l.X+l.W+l.Xres; x += l.Xres {
			s.Save()
			s.Translate(x, y+l.GetOffset(x, y, l.W, l.H))
			s.FillRectangle(-0.5, -0.5, 1, 1)
			s.Restore()
		}
	}
	s.Restore()
}
