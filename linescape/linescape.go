package linescape

import (
	"github.com/bit101/blgo"
	"github.com/bit101/blgo/color"
)

type LineScapeOffset func(x, y, w, h float64) float64

type Linescape struct {
	X         float64
	Y         float64
	W         float64
	H         float64
	Xres      float64
	Yres      float64
	BgColor   color.Color
	FgColor   color.Color
	GetOffset LineScapeOffset
}

func NewLinescape(x, y, w, h, xres, yres float64, bgColor, fgColor color.Color, getOffset LineScapeOffset) *Linescape {
	return &Linescape{
		X:         x,
		Y:         y,
		W:         w,
		H:         h,
		Xres:      xres,
		Yres:      yres,
		BgColor:   bgColor,
		FgColor:   fgColor,
		GetOffset: getOffset,
	}
}

func (l *Linescape) Render(s *blgo.Surface) {
	s.Save()
	s.Rectangle(l.X, l.Y, l.W, l.H)
	s.Clip()
	for y := l.Y; y < l.H+l.H+l.Yres; y += l.Yres {
		for x := l.X; x < l.X+l.W+l.Xres; x += l.Xres {
			s.LineTo(x, y+l.GetOffset(x, y, l.W, l.H))
		}
		s.SetSourceColor(l.FgColor)
		s.StrokePreserve()
		s.LineTo(l.X+l.W, l.Y+l.H)
		s.LineTo(0, l.Y+l.H)
		s.SetSourceColor(l.BgColor)
		s.Fill()
	}
	s.Restore()
}
